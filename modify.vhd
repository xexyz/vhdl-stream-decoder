library	ieee;
use ieee.std_logic_1164.all;  
use ieee.std_logic_arith.all;			   
use ieee.std_logic_unsigned.all;   

entity Modify is
port (  act		: 	in std_logic;
		data	: 	in std_logic_vector(31 downto 0);
		en		:	out std_logic;
		q   	: 	out std_logic_vector(15 downto 0)
);
end Modify;


architecture behv of Modify is	

begin 

	process (act)
	begin
	
	if act = '1' then
		en <= '1';		   	
					  
		q(0) <= data(16); 
		q(1) <= data(17);
		q(2) <= data(18); 
		q(3) <= data(19); 
		q(4) <= data(20);
		q(5) <= data(21); 
		q(6) <= data(22);
		q(7) <= data(23); 
		q(8) <= data(24);
		q(9) <= data(25); 
		q(10) <= data(26);
		q(11) <= data(27); 
		q(12) <= data(28);
		q(13) <= data(29); 
		q(14) <= data(30);
		q(15) <= data(31); 
		
	else 
		en <= '0';
		
	end if;
	end process;					   	
end behv;