library	ieee;
use ieee.std_logic_1164.all;  
use ieee.std_logic_arith.all;			   
use ieee.std_logic_unsigned.all;   

entity Parser is
port (	data	: 	in std_logic_vector(31 downto 0);
		q   	: 	out std_logic_vector(31 downto 0);
		add		:	out std_logic;
		modifiy :	out std_logic;
		delete	: 	out std_logic
);
end Parser;

architecture behv of Parser is	

begin 

	q(31 downto 0) <= data(31 downto 0); 
	
	process (data)
	begin
	
	if data(15 downto 0) = "1100100" then
		add <= '1';
		modifiy <= '0';
		delete <= '0';	   	
					  
	elsif data(15 downto 0) = "1100101" then
		add <= '0';
		modifiy <= '1';
		delete <= '0';	
		
	elsif data(15 downto 0) = "1100110" then
		add <= '0';
		modifiy <= '0';
		delete <= '1';	
	
	else
		add <= '0';
		modifiy <= '0';
		delete <= '0';	
		
	end if;
	end process;					   	
end behv;