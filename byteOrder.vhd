library	ieee;
use ieee.std_logic_1164.all;  
use ieee.std_logic_arith.all;			   
use ieee.std_logic_unsigned.all;   

entity byteOrder is
port ( 	data	: 	in std_logic_vector(31 downto 0);
		q   	: 	out std_logic_vector(31 downto 0)
);
end byteOrder;

architecture behv of byteOrder is	

begin 

	
		q(0) <= data(31); 
		q(1) <= data(30);
		q(2) <= data(29); 
		q(3) <= data(28); 
		q(4) <= data(27);
		q(5) <= data(26); 
		q(6) <= data(25);
		q(7) <= data(24); 
		q(8) <= data(23);
		q(9) <= data(22); 
		q(10) <= data(21);
		q(11) <= data(20); 
		q(12) <= data(19);
		q(13) <= data(18); 
		q(14) <= data(17);
		q(15) <= data(16); 
		q(16) <= data(15); 
		q(17) <= data(14);
		q(18) <= data(13); 
		q(19) <= data(12); 
		q(20) <= data(11);
		q(21) <= data(10); 
		q(22) <= data(9);
		q(23) <= data(8); 
		q(24) <= data(7);
		q(25) <= data(6); 
		q(26) <= data(5);
		q(27) <= data(4); 
		q(28) <= data(3);
		q(29) <= data(2); 
		q(30) <= data(1);
		q(31) <= data(0); 	


					   	
end behv;